Toolviews
=========

Report analytic data on Toolforge webservice usage.

Data collection
---------------

Data collection is done using the [toolviews.py][] script which is stored
in the operations/puppet.git repo and not here. This script expects to be
given one or more Nginx access log files to parse. The log files are expected
to be using the "vhost" logging format used by Wikimedia's dynamicproxy
reverse proxy service:

```nginx
  ##
  # Logging Settings
  ##
  log_format vhosts '$host $remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"';
  access_log /var/log/nginx/access.log vhosts;
```

Note that toolviews.py requires the presence of a database in toolsdb;
the schema for that database is described by [schema.sql][].

License
-------
[GPL-3.0-or-later](https://www.gnu.org/copyleft/gpl.html "GNU GPL 3.0 or later")

[toolviews.py]: https://gerrit.wikimedia.org/r/plugins/gitiles/operations/puppet/+/refs/heads/production/modules/toolforge/files/toolviews.py
[schema.sql]: https://gitlab.wikimedia.org/toolforge-repos/toolviews/-/blob/main/schema.sql
