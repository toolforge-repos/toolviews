# -*- coding: utf-8 -*-
#
# This file is part of Toolviews
#
# Copyright (C) 2023 Wikimedia Foundation and contributors
# All Rights Reserved.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
from app import app


def test_routes():
    adapter = app.url_map.bind("")

    urls = [
        # static routes
        "/",
        # API routes
        "/api/",
        "/api/v1/tools",
        # single day of data
        "/api/v1/day/2023-03-01",
        "/api/v1/unique/day/2023-03-01",
        "/api/v1/tool/demo-unicorn/day/2023-03-01",
        "/api/v1/unique/tool/demo-unicorn/day/2023-03-01",
        # range of days of data
        "/api/v1/daily/2023-03-01/2023-03-31",
        "/api/v1/unique/daily/2023-03-01/2023-03-31",
        "/api/v1/tool/demo-unicorn/daily/2023-03-01/2023-03-31",
        "/api/v1/unique/tool/demo-unicorn/daily/2023-03-01/2023-03-31",
    ]

    for url in urls:
        assert adapter.match(url), url
