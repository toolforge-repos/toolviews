---
openapi: 3.0.2
info:
  title: Toolviews API
  description: >
    View analytic data about usage of Toolforge tools. Data is collected from
    the webserver access logs for https://*.toolforge.org/.
  license:
    name: GPL-3.0-or-later
    url: https://www.gnu.org/licenses/gpl-3.0.en.html
  version: 1.0.0
tags:
  - name: Hits
    description: Hit counts
  - name: Unique
    description: Unique daily visitor counts based on IP address
  - name: Metadata
    description: Metadata lookups
paths:
  /api/v1/daily/{start}/{end}:
    get:
      tags:
        - Hits
      summary: Hit counts for all tools in a date range
      operationId: get_hits_for_daterange
      parameters:
        - $ref: "#/components/parameters/StartParam"
        - $ref: "#/components/parameters/EndParam"
      responses:
        200:
          description: Hit counts for all tools in a date range
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDateRange"
  /api/v1/day/{date}:
    get:
      tags:
        - Hits
      summary: Hit counts for all tools on a day
      operationId: get_hits_for_date
      parameters:
        - $ref: "#/components/parameters/DateParam"
      responses:
        200:
          description: Hit counts for all tools on a day
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDate"
  /api/v1/tool/{tool}/day/{date}:
    get:
      tags:
        - Hits
      summary: Hit counts for a tool on a day
      operationId: tool_get_hits_for_date
      parameters:
        - $ref: "#/components/parameters/toolName"
        - $ref: "#/components/parameters/DateParam"
      responses:
        200:
          description: Hit counts for a tool on a day
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDate"
  /api/v1/tool/{tool}/daily/{start}/{end}:
    get:
      tags:
        - Hits
      summary: Hit counts for a tool in a date range
      operationId: tool_get_hits_for_daterange
      parameters:
        - $ref: "#/components/parameters/toolName"
        - $ref: "#/components/parameters/StartParam"
        - $ref: "#/components/parameters/EndParam"
      responses:
        200:
          description: Hit counts for all tools in a date range
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDateRange"
  /api/v1/unique/daily/{start}/{end}:
    get:
      tags:
        - Unique
      summary: Unique IP hit counts for all tools in a date range
      operationId: get_unique_hits_for_daterange
      parameters:
        - $ref: "#/components/parameters/StartParam"
        - $ref: "#/components/parameters/EndParam"
      responses:
        200:
          description: Unique IP hit counts for all tools in a date range
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDateRange"
  /api/v1/unique/day/{date}:
    get:
      tags:
        - Unique
      summary: Unique IP hit counts for all tools on a day
      operationId: get_unique_hits_for_date
      parameters:
        - $ref: "#/components/parameters/DateParam"
      responses:
        200:
          description: Unique IP hit counts for all tools on a day
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDate"
  /api/v1/unique/tool/{tool}/day/{date}:
    get:
      tags:
        - Unique
      summary: Unique IP hit counts for a tool on a day
      operationId: tool_get_unique_hits_for_date
      parameters:
        - $ref: "#/components/parameters/toolName"
        - $ref: "#/components/parameters/DateParam"
      responses:
        200:
          description: Unique IP hit counts for a tool on a day
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDate"
  /api/v1/unique/tool/{tool}/daily/{start}/{end}:
    get:
      tags:
        - Unique
      summary: Unique daily IP visitor counts for a tool in a date range
      operationId: tool_get_unique_hits_for_daterange
      parameters:
        - $ref: "#/components/parameters/toolName"
        - $ref: "#/components/parameters/StartParam"
        - $ref: "#/components/parameters/EndParam"
      responses:
        200:
          description: Unique IP hit counts for all tools in a date range
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HitsForDateRange"
  /api/v1/tools:
    get:
      tags:
        - Metadata
      summary: Get all tool names
      operationId: get_tools
      parameters: []
      responses:
        200:
          description: List of all known tools
          content:
            application/json:
              schema:
                type: object
                properties:
                  results:
                    type: array
                    items:
                      type: string
                    example:
                      - admin
                      - openstack-browser
                      - toolviews
components:
  parameters:
    DateParam:
      name: date
      description: Date in ISO 8601 format (YYYY-MM-DD)
      in: path
      required: true
      schema:
        type: string
        format: date
      example: "2023-03-01"
    StartParam:
      name: start
      description: First date to get hits for (YYYY-MM-DD)
      in: path
      required: true
      schema:
        type: string
        format: date
      example: "2023-03-01"
    EndParam:
      name: end
      description: Last date to get hits for (YYYY-MM-DD)
      in: path
      required: true
      schema:
        type: string
        format: date
      example: "2023-03-31"
    toolName:
      name: tool
      description: Name of tool
      in: path
      required: true
      schema:
        type: string
        example: toolviews
  schemas:
    HitsForDate:
      type: object
      properties:
        date:
          type: string
          format: date
        tool:
          type: string
          example: "*"
        results:
          type: object
          additionalProperties:
            type: integer
          example:
            toolA: 1234
            toolB: 5678
            toolC: 9876
    HitsForDateRange:
      type: object
      properties:
        start:
          type: string
          format: date
          example: "2023-03-01"
        end:
          type: string
          format: date
          example: "2023-03-31"
        tool:
          type: string
          example: "*"
        results:
          type: object
          additionalProperties:
            type: object
            additionalProperties:
              type: integer
          example:
            "2023-03-01":
              toolA: 1234
              toolB: 5678
              toolC: 9876
            "2023-03-02":
              toolA: 1234
              toolB: 5678
              toolC: 9876
