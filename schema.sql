-- Toolviews schema
--
-- Copyright (c) 2018 Wikimedia Foundation and contributors
-- All Rights Reserved.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE TABLE `daily_raw_views` (
    `tool` VARCHAR(128) NOT NULL COMMENT 'Tool name',
    `request_day` DATE NOT NULL COMMENT 'Day that requests were made',
    `hits` INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '2xx responses counted',
    `uniqueiphits` INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '2xx responses counted to unique requesting IPs',
    PRIMARY KEY (`tool`, `request_day`),
    KEY (`request_day`)
) CHARACTER SET=latin1 ENGINE=InnoDB ROW_FORMAT=DYNAMIC;


# This is basically a cache table used to generate unique views; historic data
#  should be cleared out periodically.
CREATE TABLE `daily_ip_views` (
    `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
    `tool` VARCHAR(128) NOT NULL COMMENT 'Tool name',
    `request_day` DATE NOT NULL COMMENT 'Day that requests were made',
    `ip_hash` varbinary(128) NOT NULL COMMENT 'ip address hash',
    PRIMARY KEY (`id`),
    KEY (`request_day`)
) CHARACTER SET=latin1 ENGINE=InnoDB ROW_FORMAT=DYNAMIC;
