# -*- coding: utf-8 -*-
#
# This file is part of Toolviews
#
# Copyright (C) 2019 Wikimedia Foundation and contributors
# All Rights Reserved.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
import operator

import pymysql.cursors
import toolforge

from . import cache


def dbconn():
    return toolforge.toolsdb(dbname="s53734__toolviews_p")


def get_results(sql, params=None):
    """Execute an SQL query and yield rows."""
    conn = dbconn()
    try:
        with conn.cursor(pymysql.cursors.SSDictCursor) as cur:
            cur.execute(sql, params)
            for row in cur:
                yield row
    finally:
        conn.close()


def hits(date, tool="*"):
    date_str = date.strftime("%Y-%m-%d")
    key = "hits:{}:{}".format(tool, date_str)
    data = cache.CACHE.load(key)
    if data is None:
        sql = (
            "SELECT hits, uniqueiphits, tool "
            "FROM daily_raw_views "
            "WHERE request_day = %s "
        )
        params = (date_str,)
        if tool != "*":
            sql += "AND tool = %s "
            params += (tool,)
        data = list(get_results(sql, params))
        data.sort(key=operator.itemgetter("hits"), reverse=True)
        cache.CACHE.save(key, data, 3600)
    return data


def hits_range(start, end, tool="*"):
    start_str = start.strftime("%Y-%m-%d")
    end_str = end.strftime("%Y-%m-%d")
    key = "hits:{}:{}:{}".format(tool, start_str, end_str)
    data = cache.CACHE.load(key)
    if data is None:
        sql = (
            "SELECT request_day, tool, hits, uniqueiphits "
            "FROM daily_raw_views "
            "WHERE request_day >= %s "
            "AND request_day <= %s "
        )
        params = (
            start_str,
            end_str,
        )
        if tool != "*":
            sql += "AND tool = %s "
            params += (tool,)
        # Convert datatime.datetime data from the db to ISO8601 strings
        data = [
            {
                "date": row["request_day"].strftime("%Y-%m-%d"),
                "tool": row["tool"],
                "hits": row["hits"],
                "uniqueiphits": row["uniqueiphits"],
            }
            for row in get_results(sql, params)
        ]
        data.sort(key=operator.itemgetter("hits"), reverse=True)
        data.sort(key=operator.itemgetter("date"), reverse=True)
        cache.CACHE.save(key, data, 3600)
    return data


def list_tools():
    key = "tools:list"
    data = cache.CACHE.load(key)
    if data is None:
        sql = "SELECT DISTINCT(tool) FROM daily_raw_views"
        data = list(get_results(sql))
        data.sort(key=operator.itemgetter("tool"))
        cache.CACHE.save(key, data, 3600)
    return data
